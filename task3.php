<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Chess Board</title>
    <style>
        .content{
            width: 800px;
            height: 800px;
            margin: 0 auto;
            font-size: 0;

        }
        .box-black{
            background-color: #000;
            height: 100px;
            width: 100px;
            display:inline-block;
            
            
        }   
        .box-white{
            background-color: teal;
            height: 100px;
            width: 100px;
            display: inline-block;

        }

    </style>
</head>
<body>
    <div class="content">
    <?php
        
        for($i=1;$i<=8;$i++){
            
            for($j=1;$j<=8;$j++){
                
                if($i&1){
                    
                    if($j&1){ //Nested if-else #Start
                        echo '<div class="box-black"></div>';
                    }
                    else {
                        echo '<div class="box-white"></div>';
                    } //Nested if-else #End

                } //IF 
                else {
                    if($j&1){ //Nested if-else #Start
                        echo '<div class="box-white"></div>';
                    }
                    else {
                        echo '<div class="box-black"></div>';
                    }   //Nested if-else #End
                } //ELSE
            }
            echo "<br>";
        }
        
    
    ?>
    </div>


</body>
</html>