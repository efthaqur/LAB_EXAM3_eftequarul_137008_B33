<?php
/**
 * Task One Lab exam 3
 * User: Md Eftequarul Alam
 * Date: 10/15/2016
 * Time: 11:14 AM
 * Prime Number (task1.php)
 */

    $file = fopen("prime_number_137008.txt", 'w+');
    $start=1;
    $end=1000;
    for($num=$start;$num<=$end;$num++){
        if($num==1)
            continue;
        $is_prime=true;
        for($i=2;$i<=ceil($num/2);$i++){
            if($num%$i==0){
                $is_prime=false;
                break;
            }
        }
        if($is_prime){
            echo $num; echo ", ";
            fwrite($file,$num.",");

        }

    }
    fclose($file);

?>

