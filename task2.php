<?php
/**
 * Task two Lab exam 3
 * User: Md Eftequarul Alam
 * Date: 10/15/2016
 * Time: 11:14 AM
 * Average Temperature (task2.php)
 */

    $temp = array(78,60,62,68,71,68,73,85,66,64,76,63,75,76,73,68,62,73,72,65,74,62,62,65,64,68,73,75,79,73);

    $arraySize=count($temp);

    $tempSum=array_sum($temp);

    $tempAvg = ($tempSum / $arraySize);

    echo "Average Temperature of this month is: ".$tempAvg."<br>";

	$uniqtemp=array_unique($temp); # Repeated data removed using this array function

    $uniqSize=count($uniqtemp);
    

    sort($uniqtemp); # Sorting Unique temp according to ascending order
	echo "Lowest Temperature:";
    for($i=0;$i<=4;$i++){
    	echo $uniqtemp[$i].", ";
    }

    echo "<br>Highest Temperature:";
    
    for($i=($uniqSize-1); $i >($uniqSize-6); $i--) { 
    	echo $uniqtemp[$i].", ";
    }

?>
